def fibonacci(noTerm):
    if noTerm==1 or noTerm==2:
        return 1 
    return fibonacci(int(noTerm)-1) + fibonacci(int(noTerm)-2)
    
noTerm=input('Choose a number: ')
# print(fibonacci(noTerm))

for i in range(1, noTerm+1):
    print(fibonacci(i))
    