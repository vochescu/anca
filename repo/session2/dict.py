#import pprint
from pprint import pprint

personDictionary={}
personDictionary['age']=18
personDictionary['name']='anca'
personDictionary['children']=[]
personDictionary['children'].append({'age':2,'name':'jim'})
personDictionary['children'].append({'age':1,'name':'anne'})

#pprint.pprint(personDictionary)
#pprint(personDictionary)

if 'n' in personDictionary:
    print(personDictionary['n'])
else:
    print('no key "n" in dictionary')
    
#print(type(personDictionary))

for key in personDictionary:
    #print(key)
    print(personDictionary[key])
if 'name' in personDictionary:
    del personDictionary['name']
for key, value in personDictionary.items():
    print(value)