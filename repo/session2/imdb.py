import requests
import json
import pprint

def searchNamesImdb(keywords):
	testTemplate = 'http://www.imdb.com/xml/find?json=1&nr=1&nm=on&q={keywords}'
	apiUrl = testTemplate.format(keywords ='+'.join(keywords))
	# print(apiUrl)
	results=[]
	response = requests.get(apiUrl)
	if response.status_code==200:
	#   # print(response.text)
		jsonResult = json.loads(response.text)
	# 	#pprint.pprint(jsonResult)
		for item in jsonResult['name_approx']:
			results.append(item['name'])
		for item in jsonResult['name_popular']:
			results.append(item['name'])
	return results
			
testKeywords = ['ewan','mcgregor']
pprint.pprint(searchNamesImdb(testKeywords))